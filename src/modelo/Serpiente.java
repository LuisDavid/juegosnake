package modelo;

import java.util.ArrayList;

public class Serpiente {
	private ArrayList<Bloque> cuerpo;
	
	private Direccion direccion; 
	public Serpiente() {
		cuerpo= new ArrayList<Bloque>();
		direccion=Direccion.IZQUIERDA;
	}
	
	
	public ArrayList<Bloque> getCuerpo() {
		return cuerpo;
	}
	public void setCuerpo(ArrayList<Bloque> cuerpo) {
		this.cuerpo = cuerpo;
	}
	public Direccion getDireccion() {
		return direccion;
	}
	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}
	public void incrementarCuerpo(Bloque b) {
		this.cuerpo.add(b);
	}
	public boolean moverSerpiente(Direccion direccion,Bloque b){
		Bloque aux1= new Bloque(cuerpo.get(0).getX(),cuerpo.get(0).getY(),cuerpo.get(0).getAncho());
		Bloque aux2;
		if(direccion!=null) {
			for(int i=1; i<cuerpo.size(); i++){
				aux2=cuerpo.get(i);
				cuerpo.set(i,aux1);
				aux1=aux2;
			}
			if(direccion== Direccion.DERECHA) {
				if(this.direccion!=Direccion.IZQUIERDA) {
					cuerpo.get(0).incrementarX(aux1.getAncho());
					this.direccion=direccion;
				}
				else cuerpo.get(0).decrementarX(aux1.getAncho());
			}
			else if(direccion== Direccion.IZQUIERDA) {
				if(this.direccion!=Direccion.DERECHA) {
					cuerpo.get(0).decrementarX(aux1.getAncho());
					this.direccion=direccion;
				}
				else cuerpo.get(0).incrementarX(aux1.getAncho());
			}
			else if(direccion== Direccion.ARRIBA) {
				if(this.direccion!=Direccion.ABAJO) {
					cuerpo.get(0).decrementarY(aux1.getAncho());
					this.direccion=direccion;
				}
				else cuerpo.get(0).incrementarY(aux1.getAncho());
			}
			else if(direccion== Direccion.ABAJO) {
				if(this.direccion!=Direccion.ARRIBA) {
					cuerpo.get(0).incrementarY(aux1.getAncho());
					this.direccion=direccion;
				}
				else cuerpo.get(0).decrementarY(aux1.getAncho());
			}
			if(b.equals(cuerpo.get(0))) {
				comer(aux1);
				return true;
			}
		}
		return false;
	}
	//Este método debe se llamado justo cuando se encuentra la serpiente sobre la comida.
	//Y se le debe de pasar el bloque que se situara en la parte de atras de la serpiente.
	public void comer(Bloque b) {
		cuerpo.add(b);
	}
}
