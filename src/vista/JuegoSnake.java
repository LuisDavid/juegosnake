package vista;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import modelo.Bloque;
import modelo.Direccion;
import modelo.Serpiente;



public class JuegoSnake extends JPanel{
	
	private Font ePuntaje;
	private Font eNivel;
	private int puntajeJugador;
	private int nivel;
	private final static int ANCHO=500;
	private final static int ALTO=400;
	
	
	private Serpiente serpiente;
	private Bloque comidaSerpiente;
	private static Direccion direccion;
	private ImageIcon[] imagenes;
	//Ancho y alto del cuadro en el que se movera la serpiente
	private int[] cuadroJuego;
	private int[] coordenadasX;
	private int[] coordenadasY;
	
	Clip sonido;
	public JuegoSnake() throws LineUnavailableException, IOException, UnsupportedAudioFileException {
		super();
		nivel=0;
		
		
		imagenes=new ImageIcon[6];
		imagenes[0] = new ImageIcon(getClass().getResource("/images/1.jpeg")); 
		imagenes[1] = new ImageIcon(getClass().getResource("/images/2.jpg")); 
		imagenes[2] = new ImageIcon(getClass().getResource("/images/3.jpg")); 
		imagenes[3] = new ImageIcon(getClass().getResource("/images/4.jpg")); 
		imagenes[4] = new ImageIcon(getClass().getResource("/images/5.jpg")); 
		imagenes[5] = new ImageIcon(getClass().getResource("/images/5.jpg")); 
		
		cuadroJuego=new int[2];
		cuadroJuego[0]=480;
		cuadroJuego[1]=300;
		
		
		coordenadasX=new int[(cuadroJuego[0]/15)];
		coordenadasY=new int[(cuadroJuego[1]/15)];
		coordenadasX[0]=5;
		for(int i=1; i<coordenadasX.length; i++) {
			coordenadasX[i]=coordenadasX[i-1]+15;
		}
		coordenadasY[0]=60;
		for(int i=1; i<coordenadasY.length; i++) {
			coordenadasY[i]=coordenadasY[i-1]+15;
		}
		
		serpiente=crearSerpiente();
		//serpiente.setSentidoDireccion(SentidoDireccion.IZQUIERDA);
		comidaSerpiente=getBloqueAleatorio();
		
		sonido= AudioSystem.getClip();
		sonido.open(AudioSystem.getAudioInputStream(new File("sounds/comer.wav")));
	//	sonido.start();
		
		
	}
	@Override
	public void paint(Graphics g) {
		super.paint(g);
	
		ePuntaje= new Font("Monospaced",Font.BOLD,18);
		eNivel= new Font("Monospaced",Font.BOLD,18);
		
		g.setFont(ePuntaje);
		g.setFont(eNivel);
		Dimension height = getSize();
		g.drawImage(imagenes[nivel].getImage(), 0, 0, height.width, height.height, null);
		g.setColor(Color.BLACK);
		g.drawString("Puntaje: "+puntajeJugador, 10,30);
		g.drawString("Nivel: "+nivel, 380,30);

		
		for(Bloque b: serpiente.getCuerpo()) {
			g.fillRoundRect(b.getX(), b.getY(), 15, 15, 20, 20);
		}
		g.fillRect(5, 40, cuadroJuego[0]+5,5);
		
		g.fillRect(5, 60, cuadroJuego[0]+5,5);//------
		g.fillRect(cuadroJuego[0]+5, 60, 5,cuadroJuego[1]);//     			  |
		g.fillRect(5, 60, 5,cuadroJuego[1]);//|
		g.fillRect(5, cuadroJuego[1]+60, cuadroJuego[0]+5,5);//-----
		
		
		g.setColor(Color.ORANGE);
		g.fillRoundRect(comidaSerpiente.getX(),comidaSerpiente.getY(), 15, 15, 20, 20);
		
	}
	public static void main(String[]args) throws InterruptedException, LineUnavailableException, IOException, UnsupportedAudioFileException {
		JFrame ventana=new JFrame("Juego de Snake");
		boolean seguirJugando=true;
		ventana.setSize(ANCHO, ALTO);
		ventana.setResizable(false);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JuegoSnake juegoSnake=new JuegoSnake();
		int velocidad=300;
		ventana.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) {}
			
			@Override
			public void keyReleased(KeyEvent arg0) {}
			
			@Override
			public void keyPressed(KeyEvent e) {
				String tecla=e.getKeyText(e.getKeyCode());
				if(tecla.equals("Izquierda")){
					direccion=Direccion.IZQUIERDA;
				}
				else if(tecla.equals("Derecha")) {
					direccion=Direccion.DERECHA;
				}
				else if(tecla.equals("Arriba")) {
					direccion=Direccion.ARRIBA;
				}
				else if(tecla.equals("Abajo")) {
					direccion=Direccion.ABAJO;
				}
				
			}
		});
		
		ventana.add(juegoSnake);
		 
		ventana.setVisible(true);
		
		while(seguirJugando) {
			juegoSnake.repaint();
			
			//juegoSnake.getSerpiente().setDireccion(direccion);
			
			if(juegoSnake.getSerpiente().moverSerpiente(direccion,juegoSnake.comidaSerpiente)) {
				juegoSnake.puntajeJugador++;
				juegoSnake.comidaSerpiente=juegoSnake.getBloqueAleatorio();
				juegoSnake.sonido.loop(1);
				juegoSnake.sonido.start();
				
			}
			juegoSnake.nivel=juegoSnake.puntajeJugador/5;
			velocidad=300-juegoSnake.nivel*50;
			if(juegoSnake.nivel==5) {
				JOptionPane.showMessageDialog(null, "Ganaste!", "Juego Snake", JOptionPane.WARNING_MESSAGE);
				seguirJugando=preguntarReinicio(juegoSnake);
			}
			if(juegoSnake.hayColision()) {
				JOptionPane.showMessageDialog(null, "Se ha terminado el juego", "Juego Snake", JOptionPane.WARNING_MESSAGE);
				seguirJugando=preguntarReinicio(juegoSnake);
		}
			Thread.sleep(velocidad);
		}
		ventana.dispose();
	}
	public static boolean preguntarReinicio(JuegoSnake juegoSnake) {
		boolean bandera=true;
		if(JOptionPane.showConfirmDialog(null, "¿Quieres volver a jugar?","Ping Pong", JOptionPane.YES_NO_OPTION)==0){
			juegoSnake.reiniciarJuego();
		}
		else {
			bandera=false;
		}
		return bandera;
	}
	public void reiniciarJuego() {
		nivel=0;
		puntajeJugador=0;
		serpiente= crearSerpiente();
		direccion=null;
	}
	public Serpiente getSerpiente() {
		return serpiente;
	}
	public void setSerpiente(Serpiente serpiente) {
		this.serpiente = serpiente;
	}
	//Verifica si hay colision con el cuerpo o con la frontera y regresa verdadero si 
	//hay una colusión.
	public boolean hayColision() {
		ArrayList<Bloque> cuerpo=serpiente.getCuerpo();
		Bloque cabeza=cuerpo.get(0);
		for(int i=1; i<cuerpo.size(); i++) {
			if(cabeza.equals(cuerpo.get(i))) {
				return true;
			}
		}
		
		if(cabeza.getX()<5 || cabeza.getX()>cuadroJuego[0]+5) {
			return true;
		}
		else if(cabeza.getY()<60 || cabeza.getY()>cuadroJuego[1]+45){
			return true;
		}
		return false;
	}
	public Bloque getBloqueAleatorio() {
		Bloque b=new Bloque();
		b.setX(coordenadasX[(int)(Math.random()*coordenadasX.length)]);
		b.setY(coordenadasY[(int)(Math.random()*coordenadasY.length)]);
		b.setAncho(15);
		return b;
	}
	public Serpiente crearSerpiente() {
		Serpiente s= new Serpiente();
		Bloque b=getBloqueAleatorio();
		if(b.getX()<60) {
			b.setX(65);
		}
		else if(b.getX()>cuadroJuego[0]-60) {
			b.setX(b.getX()-60);
		}
		
		s.incrementarCuerpo(b);
		s.incrementarCuerpo(new Bloque(b.getX()+15,b.getY(),15));
		s.incrementarCuerpo(new Bloque(b.getX()+30,b.getY(),15));
		s.incrementarCuerpo(new Bloque(b.getX()+45,b.getY(),15));
		
		return s;
	}
}
