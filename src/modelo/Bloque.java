package modelo;

public class Bloque {
	private int x;
	private int y;
	private int ancho;
	public Bloque() {
		
	}
	public Bloque(int x, int y,int ancho) {
		this.x=x;
		this.y=y;
		this.ancho=ancho;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public void incrementarX(int v) {
		x=x+v;
	}
	public void incrementarY(int v) {
		y=y+v;
	}
	public void decrementarX(int v) {
		x=x-v;
	}
	public void decrementarY(int v) {
		y=y-v;
	}
	
	public int getAncho() {
		return ancho;
	}
	public void setAncho(int ancho) {
		this.ancho = ancho;
	}
	@Override
	public boolean equals(Object o) {
		Bloque b= (Bloque)o;
		if(this.x==b.getX() && this.y==b.y) {
			return true;
		}
		return false;
	}
	@Override
	public String toString() {
		return "coor: "+x+":"+y;
	}
}
